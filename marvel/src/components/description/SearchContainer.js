import React, { Component } from 'react'
import ComicsData from './ComicsData'
import InputData from './InputData'
import md5 from 'md5'
import axios from 'axios'
import './SearchContainer.css'

//this component has the search bar from which get bring the data from the server.

class SearchContainer extends Component {
    state;
    constructor(){
        super()
        this.state = {
            keyword: '',
            comics : [],
            // loading: false,
        }
      this.getComicsFromServer = this.getComicsFromServer.bind(this)
      this.handleKeywordChange = this.handleKeywordChange.bind(this)
    }
    getComicsFromServer() {
        if (this.state.keyword){
            const timestamp = Date.now()
            const apiHashing = md5(`${timestamp}${process.env.REACT_APP_MARVEL_API_PRIVATE_KEY}${process.env.REACT_APP_MARVEL_API_PUBLIC_KEY}`)
            // this.setState({
            //     loading: true
            // })
            axios.get(`${process.env.REACT_APP_MARVEL_API_URL}${process.env.REACT_APP_MARVEL_API_COMICS_ENDPOINT}?title=${this.state.keyword}&apikey=${process.env.REACT_APP_MARVEL_API_PUBLIC_KEY}&hash=${apiHashing}&ts=${timestamp}`)
            .then(res => {
               this.setState(currentState => ({
                    //keyword: currentState.keyword,
                    //state2: currentState.keyword
                    ...currentState, // == keyword
                    comics: res.data.data.results
                }))
            })
            .catch(err => console.err(err))/*.finally(()=>this.setState({
                loading: false
            }));*/
        }
    }

    handleKeywordChange(keywordParam){
        this.setState(currentState => ({
            keyword: keywordParam,
            comics: currentState.comics
        }), () => this.getComicsFromServer())
    }
    render() {
        return (
            <div className="background">
                {this.state.loading &&<div>Loading</div>}
                 <InputData sendKeywordToApp={this.handleKeywordChange}/>
                <ComicsData comicsFromParent={this.state.comics} />
            </div>
        )
    }
}
export default SearchContainer