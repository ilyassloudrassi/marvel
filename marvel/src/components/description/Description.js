import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Description.css'

 class Description extends Component {
    constructor(){
        super()
    }
   
    render() {
        return (
            <div>
                <h1>Marvel API App</h1>
                <h5>This is a small app made with with react that brings comics details from an API call that contains the comics details such title, prices..ect</h5>
                <Link to="/search-comic" role="button"> Click here</Link>
            </div>
        )
    }
}

export default Description;
