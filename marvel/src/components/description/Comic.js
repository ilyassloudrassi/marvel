import React, { Component } from 'react'
import './Comic.css'
//this component has the comic card 
class Comic extends Component {

    render() {
        const {comic} = this.props;
        return (
            <div className="comic-card">
                <div className="comic-cover">
                    <img src={`${comic.thumbnail.path}/landscape_incredible.${comic.thumbnail.extension}`}/>
                </div>
                <div className="comic-metadata">
                    <p className="name">{comic.title}</p>
                    <p className="price">Price:<span>${comic.prices[0].price}</span></p>
                    <p className="issue-number">Issue number: <span>{comic.issueNumber}</span> </p>
                </div>
            </div>
        )
    }
}
export default Comic;






