import React, { Component } from 'react'
import './InputData.css'

//in this component where i handle the user input in the search bar and send the state to the parent component.

class InputData extends Component {
    
    constructor(){
        super();
        this.handleSearchClick = this.handleSearchClick.bind(this)
        this.handleKeywordChange = this.handleKeywordChange.bind(this)
        this.state = {
            keyword: ''
        };
    }

    handleSearchClick() {
        this.props.sendKeywordToApp(this.state.keyword);
    }

    handleKeywordChange(event){
        this.setState({keyword: event.target.value}) 
    }
    
    render() {
        return (
            <div className="search-bar">
                <h3>Enter a comic: </h3>
                <div className="form">
                    <input className="search-form" name="search" value={this.state.keyword} onChange={this.handleKeywordChange} type="text" placeholder="Ex: Iron Man.." />
                    <button   onClick={this.handleSearchClick} className="comicInput" >Search</button>
                </div>
            </div>
        )
    }
}

export default InputData;