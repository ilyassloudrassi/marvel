import React, { Component } from 'react'
import Comic from './Comic'
import './ComicsData.css'

//in this component where i handle the dynamic card data.

class ComicsData extends Component {

    //comics = []

    render() {
        //this.comics = this.props.comicsFromParent
        return (
            <div className="container">
                <div className="comic-content">
                    {this.props.comicsFromParent.map((comic) => (
                        <div key = {comic.id}> 
                            <Comic comic={comic}/> 
                        </div>
                    ))}
                </div>
            </div>
        )
    }
}

export default ComicsData;



