import React, { Component } from 'react'
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Description from './components/description/Description';
import SearchContainer from './components/description/SearchContainer';

export default class App extends Component {
 
    render() {
        return (
            <Router>
                <div >
                    <Switch>
                        <Route exact path="/" component={Description}/>
                        <Route exact path="/search-comic" component={SearchContainer}/>
                    </Switch>
                </div>
            </Router>
        )
    }
}
